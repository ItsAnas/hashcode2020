#include <vector>
#include <iostream>


void debug_vect(std::vector<int> t)
{
    for (auto i = t.begin(); i != t.end(); i++)
    {
        std::cout << *i << " - ";
    }

    std::cout << '\n';
}


class library
{
public:
    library(int id, int books, int signupTime, int maxShip)
    : books_{books}
    , signupTime_{signupTime}
    , maxShip_{maxShip}
    , id_{id}
    {}

    void debug()
    {
        std::cout << "L:" << id_ << " B:" << books_ << " signupT: " << signupTime_ << " maxship: " << maxShip_ << '\n'; 
        std::cout << "Books are: ";
        debug_vect(bookList_);
    }

    std::vector<int> bookList_;
    int books_;
    int signupTime_;
    int maxShip_;
    int id_;
};