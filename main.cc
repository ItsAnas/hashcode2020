#include <iostream>
#include <fstream> // std::ifstream
#include <vector>
#include <string>
#include <sstream>
#include <set>
#include <algorithm>

#include "library.hh"

int total_books;
int total_libraries;
int total_days;
std::vector<bool> signedbooks;
int days = 0;

std::vector<std::string> split(std::string str, char delimiter)
{
    std::vector<std::string> internal;
    std::stringstream ss(str); // Turn the string into a stream.
    std::string tok;

    while (std::getline(ss, tok, delimiter))
    {
        internal.push_back(tok);
    }

    return internal;
}

std::vector<int> convert(std::vector<std::string> string_vec)
{
    std::vector<int> vect;

    for (auto it = string_vec.begin(); it != string_vec.end(); it++)
    {
        vect.push_back(std::stoi(*it));
    }

    return vect;
}

void init(std::ifstream &file, std::string line)
{
    std::getline(file, line);

    std::vector firstLine = split(line, ' ');
    total_books = std::stoi(firstLine[0]);
    total_libraries = std::stoi(firstLine[1]);
    total_days = std::stoi(firstLine[2]);
}

std::vector<int> books_score; // index == id

std::vector<library> libraries;

void parse_data(std::ifstream &file)
{
    std::string line;

    bool is_library = true;
    int id = 0;

    while (std::getline(file, line))
    {
        if (line.size() == 0)
        {
            break;
        }

        auto lineVect = split(line, ' ');
        std::vector<int> lineVectInt = convert(lineVect);

        library l(id++, lineVectInt[0], lineVectInt[1], lineVectInt[2]);
        std::getline(file, line);

        lineVect = split(line, ' ');
        lineVectInt = convert(lineVect);

        l.bookList_ = lineVectInt;
        libraries.push_back(l);
    }
}

// A pred function to adjust according to your score
bool comparator(const library &s1, const library &s2)
{
    if (days + s1.signupTime_ > total_days)
    {
        if (days + s2.signupTime_ > total_days)
        {
            return false;
        }
    }

    if (days + s2.signupTime_ > total_days)
    {
        if (days + s1.signupTime_ > total_days)
        {
            return true;
        }
    }

    int maxScore1 = 0;
    int nbBook1 = 0;

    for (auto it = s1.bookList_.begin(); it != s1.bookList_.end(); it++)
    {
        if (!signedbooks[*it])
        {
            maxScore1 += books_score[*it];
            nbBook1++;
        }
    }

    int maxScore2 = 0;
    int nbBook2 = 0;

    for (auto it = s2.bookList_.begin(); it != s2.bookList_.end(); it++)
    {
        if (!signedbooks[*it])
        {
            maxScore2 += books_score[*it];
            nbBook2++;
        }
    }

    double ratio1 = maxScore1 / (s1.signupTime_ + (nbBook1 / s1.maxShip_));
    double ratio2 = maxScore2 / (s2.signupTime_ + (nbBook2 / s2.maxShip_));

    return ratio1 < ratio2;
}

std::vector<library>::iterator find_last_resort()
{
    for (auto it = libraries.begin(); it != libraries.end(); it++)
    {
        library l = *it;

        if (days + l.signupTime_ < total_days)
        {
            return it;
        }
    }

    return libraries.end();
}

void main_algo()
{
    int total_library_sign = 0;

    while (true)
    {
        std::sort(libraries.begin(), libraries.end(), [](library s1, library s2) {
            int maxScore1 = 0;

            for (auto it = s1.bookList_.begin(); it != s1.bookList_.end(); it++)
            {
                if (!signedbooks[*it])
                {
                    maxScore1 += books_score[*it];
                }
            }

            int maxScore2 = 0;

            for (auto it = s2.bookList_.begin(); it != s2.bookList_.end(); it++)
            {
                if (!signedbooks[*it])
                {
                    maxScore2 += books_score[*it];
                }
            }

            double ratio1 = maxScore1 / (s1.signupTime_ + (s1.bookList_.size() / s1.maxShip_));
            double ratio2 = maxScore2 / (s2.signupTime_ + (s2.bookList_.size() / s2.maxShip_));

            return ratio1 > ratio2;
        });

        auto it = libraries.begin();

        if (it == libraries.end())
        {
            break;
        }

        library l = *it;

        if (days + l.signupTime_ > total_days)
        {
            libraries.erase(it);
            continue;
        }

        total_library_sign++;

        int current_max_ship;

        current_max_ship = l.bookList_.size();

        std::cout << l.id_ << ' ' << current_max_ship << '\n';

        int total_ship = 0;

        for (auto it = l.bookList_.begin(); it != l.bookList_.end() && total_ship < current_max_ship; total_ship++, it++)
        {
            std::cout << *it << ' ';
        }

        for (auto it = l.bookList_.begin(); it != l.bookList_.end(); it++)
        {
            signedbooks[*it] = true;
        }

        libraries.erase(it);


        std::cout << '\n';
    }

    std::cout << total_library_sign << '\n';
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        std::cerr << "No input file\n";
    }

    std::ifstream file(argv[1]);
    std::string line;

    if (!file.is_open())
    {
        std::cerr << "file is not open\n";
    }

    init(file, line);
    std::getline(file, line);
    std::vector secondLine = split(line, ' ');

    for (auto it = secondLine.begin(); it != secondLine.end(); it++)
    {
        books_score.push_back(std::stoi(*it));
    }

    signedbooks = std::vector<bool>(total_books, false);

    parse_data(file);
    main_algo();

    return 0;
}
